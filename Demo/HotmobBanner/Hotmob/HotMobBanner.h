//
//  Created by HotMob on 12-10-16.
//  Copyright (c) 2012. All rights reserved.
//
//  HotMob iOS SDK 3.0.2
//  Release date: 27 Sept, 2013

#import <UIKit/UIKit.h>

@class HotMobBanner;

typedef enum {
    HotmobBannerTypeNormal = 0,
    HotmobBannerTypePopup,
    HotmobBannerTypeVertical
} HotmobBannerType;

typedef enum {
    ShowUpTypeFromNone = 0,
    ShowUpTypeFromTop,
    ShowUpTypeFromBottom,
    ShowUpTypeFullScreen,
    ShowUpTypeOneThirdFromRight
} ShowUpType;

typedef enum {
    HotmobStatusInit = 0,
    HotmobStatusLoading,
    HotmobStatusLoaded
} HotmobStatus;

@protocol HotMobBannerDelegate <NSObject>
@optional
- (void)didLoadBanner:(HotMobBanner*)banner;
- (void)didShowBanner:(HotMobBanner*)banner;
- (void)didHideBanner:(HotMobBanner*)banner;
- (void)didShowInAppBrowser:(HotMobBanner*)banner;
- (void)didCloseInAppBrowser:(HotMobBanner*)banner isCloseByUser:(BOOL)isCloseByUser;
- (void)openInternalCallback:(HotMobBanner*)banner url:(NSString*)url;
- (void)openNoAdCallback:(HotMobBanner*)banner;
@end

@interface HotMobBanner : UIView
{
    id<HotMobBannerDelegate> delegate;
    UIViewController* rootController;
    
    UIWebView* creativeWebView;
    NSTimer* refreshTimer;
    
    int refreshInterval;
    
    BOOL linkClickWebContentTab;
    BOOL linkClickInAppBrowserWebContentTab;
    HotmobStatus status;
    BOOL isShown;
    BOOL isInAppBrowserShown;
    
    HotmobBannerType type;
}

@property(nonatomic, assign)id<HotMobBannerDelegate> delegate;
@property(nonatomic, retain)NSString *adCode;
@property(nonatomic, retain)NSString *appID;
@property(assign, nonatomic)double fadeOutTimeInterval;
@property(assign, readonly)HotmobStatus status;
@property(assign, readonly)BOOL isShown;
@property(assign, readonly)BOOL isInAppBrowserShown;
@property(assign, readonly)HotmobBannerType type;
@property(nonatomic, assign)BOOL forceLandscape;
@property(nonatomic, retain)NSString *phoneCallMsg;
@property(nonatomic, retain)NSString *SMSMsg;
@property(nonatomic, retain)NSString *externalBrowserMsg;
@property(nonatomic, retain)NSString *iCalMsg;
@property(nonatomic, retain)NSString *okButtonTitle;
@property(nonatomic, retain)NSString *cancelButtonTitle;
@property(nonatomic, retain)UIWebView* creativeWebView;
@property(nonatomic, assign)BOOL debug;


+ (id)hotMobBanner:(id<HotMobBannerDelegate>)delegate
    rootController:(UIViewController*)aController
              type:(ShowUpType)aType
       bannerFrame:(CGRect)aFrame
             appID:(NSString*)appID
            adCode:(NSString *)aId;

+ (id)hotMobBanner:(id<HotMobBannerDelegate>)delegate
    rootController:(UIViewController*)aController
              type:(ShowUpType)aType
       bannerFrame:(CGRect)aFrame
             appID:(NSString*)appID
            adCode:(NSString *)aId
      phoneCallMsg:(NSString*)msg0
            SMSMsg:(NSString*)msg1
externalBrowserMsg:(NSString*)msg2
           iCalMsg:(NSString*)msg3
     okButtonTitle:(NSString*)title0
 cancelButtonTitle:(NSString*)title1;

+ (void)closeInAppBrowser;
- (void)manualRefresh;
- (void)startAutoReloadWithInterval:(int)interval;
- (void)stopAutoReload;
- (void)removeFromUIWindow;
- (UIButton*)createCloseButton:(id)target selector:(SEL)selector;

@end


