//
//  BannerAdsViewController.h
//  PMAdRequestAdapter
//
//  Created by Soon Leung on 6/3/14.
//  Copyright (c) 2014 Pixel Media HK Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMAdUnit.h"
#import "PMAdBanner.h"
#import "HotMobBanner.h"

@interface BannerAdsViewController : UITableViewController <PMAdUnitDelegate, PMAdRequestSource>

@end
