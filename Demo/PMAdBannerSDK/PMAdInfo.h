//
//  PMAdInfo.h
//  PMAdBanner
//
//  Created by Soon Leung on 3/4/14.
//  Copyright (c) 2014 n/a. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PMAdBanner.h"

@interface PMAdInfo : NSObject

#if __has_feature(objc_arc)
@property (nonatomic, strong) NSURL          *activitytTrackerURL;
@property (nonatomic, strong) NSURL          *clickTrackerURL;
@property (nonatomic, strong) NSURL          *viewTrackerURL;
@property (nonatomic, strong) NSURL          *bannerContentURL;
@property (nonatomic, strong) NSURL          *expendedContentURL;
@property (nonatomic, strong) NSURL          *otherURL;
#else
@property (nonatomic, retain) NSURL          *activitytTrackerURL;
@property (nonatomic, retain) NSURL          *clickTrackerURL;
@property (nonatomic, retain) NSURL          *viewTrackerURL;
@property (nonatomic, retain) NSURL          *bannerContentURL;
@property (nonatomic, retain) NSURL          *expendedContentURL;
@property (nonatomic, retain) NSURL          *otherURL;
#endif

@property (nonatomic)       PMBannerType            bannerType;
@property (nonatomic)       PMBannerContentType     bannerContentType;
@property (nonatomic)       PMExtenedContentType    expendedContentType;
@property (nonatomic)       PMBannerAlignment       bannerAlignment_type;
@property (nonatomic)       CGFloat                 bannerAlignment_x;
@property (nonatomic)       CGFloat                 bannerAlignment_y;
@property (nonatomic)       UIModalTransitionStyle  transitionStyle;
@property (nonatomic)       NSUInteger              bannerWidth;
@property (nonatomic)       NSUInteger              bannerHeight;
@property (nonatomic)       NSUInteger              autoDismiss;
@property (nonatomic)       BOOL                    hasCloseButton;
@property (nonatomic)       BOOL                    userInteractionEnabled;
@property (nonatomic, copy) NSString                *bannerUID;
@property (nonatomic, copy) NSString                *bannerContentURLString;

- (void)setBannerTypeByValue:(NSString *)value;
- (void)setBannerContentTypeByValue:(NSString *)value;
- (void)setExpendedContentTypeByValue:(NSString *)value;
- (void)setAlignmentTypeByValue:(NSString *)value;
- (void)setTransitionStyleByValue:(NSString *)value;

- (CGPoint)overlayPosition:(UIView*)parent withChild:(UIView*)child;
- (NSString *)otherURLString;

- (void)reset;

@end
