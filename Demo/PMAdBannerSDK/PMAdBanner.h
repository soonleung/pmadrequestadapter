//
//  PMAdBanner.h
//  PMAdBanner
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 PMAdBannerDelegate Protocol Reference
 */

@class PMAdBanner;
@protocol PMAdBannerDelegate <NSObject>

/**
 Tells the delegate that banner has loaded the banner
 @param banner The banner object
 */
- (void)bannerViewDidLoadAd:(PMAdBanner *)banner;

/**
 Ask the delegate if the application can be left after clicking the banner
 @param willLeave A boolean request from banner to execute the click action
 @returns A boolean returns on application
 */
- (BOOL)bannerViewActionShouldBegin:(PMAdBanner *)banner willLeaveApplication:(BOOL)willLeave;

/**
 Tells the delegate that banner has dismissed from the view
 @param banner The banner object
 */
- (void)bannerViewActionDidFinish:(PMAdBanner *)banner;

/**
 Tells the delegate that banner has dismissed from the view
 @param banner The banner object
 @param isPopupFromBanner A boolean to indicate the popup is dismiss from a banner ad
 */
- (void)bannerViewActionDidFinish:(PMAdBanner *)banner isPopupFromBanner:(BOOL)isPopupFromBanner;

/**
 Tells the delegate that banner error during loading
 @param error The error during loading
 */
- (void)bannerView:(PMAdBanner *)banner didFailToReceiveAdWithError:(NSError *)error;

@end

/**
 An enumeration of banner transition animation
 */

typedef enum
{
    PMBannerAnimationTransitionPushFromTop,
    PMBannerAnimationTransitionPushFromBottom,
    PMBannerAnimationTransitionFlipFromLeft,
    PMBannerAnimationTransitionFlipFromRight    
    
} PMBannerAnimationTransition;

/**
 An enumeration of banner type
 */

typedef enum 
{
    PMBannerTypeBannerAd,
    PMBannerTypeCellAd,
    PMBannerTypeCrazyAd,
    PMBannerTypeOverlayAd,
    PMBannerTypeSplashAd,
    PMBannerTypeVideoAd,
    PMBannerTypeAudioAd,
    PMBannerTypeUnknown
    
} PMBannerType;
/**
 An enumeration of PMBanner Content Type
 */

//-----------------------------------------//
typedef enum
{
    PMBannerContentTypeImage,
    PMBannerContentTypeUrl,
    PMBannerContentTypehtml
    
} PMBannerContentType;

/**
 An enumeration of extended banner type
 */

typedef enum 
{
    PMExtenedContentTypeVideo,
    PMExtenedContentTypeAudio,
    PMExtenedContentTypeCallLink,
    PMExtenedContentTypeAppStore,
    PMExtenedContentTypeInternal,
    PMExtenedContentTypeInternalCrazy,
    PMExtenedContentTypeInternalSplash,
    PMExtenedContentTypeExternal,
    PMExtenedContentTypeUnknown
    
} PMExtenedContentType;

/**
 An enumeration of overlay banner alignment
 */

//-----------------------------------------//
typedef enum
{
    PMBannerAlignmentTopLeft,
    PMBannerAlignmentTopRight,
    PMBannerAlignmentBottomLeft,
    PMBannerAlignmentBottomRight,
    PMBannerAlignmentCenter,
    PMBannerAlignmentUnknown
    
} PMBannerAlignment;

/**
 An enumeration of debug
 */

//-----------------------------------------//
typedef enum
{
    PMBannerDebugPublisher,
    PMBannerDebugDeveloper
    
} PMBannerDebug;

/**
 A class to represent the Pixel Ad banner
 */

@class PMAdInfo;
@interface PMAdBanner : NSObject
{

}


/////////////////////////////////////////////////////////////////////////////
/// @name Properties
/////////////////////////////////////////////////////////////////////////////

#if __has_feature(objc_arc)
@property (nonatomic, weak) id<PMAdBannerDelegate>  delegate;
@property (nonatomic, readonly, strong) PMAdInfo    *adInfo;
@property (nonatomic, readonly, strong) UIView      *view;
#else
@property (nonatomic, assign) id<PMAdBannerDelegate>    delegate;
@property (nonatomic, readonly, retain) PMAdInfo        *adInfo;
@property (nonatomic, readonly, retain) UIView          *view;
#endif

@property (nonatomic, copy) NSString *bannerID;
@property (nonatomic, copy) NSString *identifier;

/////////////////////////////////////////////////////////////////////////////
/// @name Class Methods
/////////////////////////////////////////////////////////////////////////////

+ (UIImage *)bundleImagePath:(NSString *)kimgName ofType:(NSString *)ktype;


/////////////////////////////////////////////////////////////////////////////
/// @name Instance Methods
/////////////////////////////////////////////////////////////////////////////
    
/**
 Initialize a new banner
 @param kbannerid The Banner ID
 @param kdelegate The delegate of object
 @returns a newly initialized object
 */
- (instancetype)initWithBannerID:(NSString *)kbannerid andDelegate:(id)kdelegate;

/**
 Start load the banner
 */
- (void)loadBanner;

/**
 Cancel load Banner
 */
- (void)CancelBanner;
- (void)askForPendingAction:(NSURL *)kurl forExtendedType:(PMExtenedContentType)kcontentType;
- (void)makeClickTrackingRequest;
- (void)makeloggingRequest:(NSURL *)URL;
- (NSString *)getTimeStamp;

/*
 Reload the current banner with banner ID
 @param kbannerid Banner ID
 */
- (void)reloadWithBannerID:(NSString *)kbannerid;

/**
 Set the ID of current banner
 @param kbannerID Banner ID 
 */
- (void)setBannerID:(NSString *)kbannerID;

/**
 Set the view to add the banner on it
 @param kview View that will add with the banner
 */
- (void)showBannerInView:(UIView *)kview;

/**
 Set the viewcontroller to banner for control the content
 @param kviewController The viewcontroller to show banner
 */
- (void)showPoppedContentFromViewController:(UIViewController *)kviewController;

/**
 Enable click through on overlay Ad
 @param status Either enable or disable
 */
- (void)enableOverlayAdTransparentClick:(BOOL)status;

/**
 Set the debug mode
 @param debug The enumeration of debug role
 */
-(void)setDebug:(PMBannerDebug)debug;

/**
 Set the GPS data to the banner
 @param latitude The latitude of location
 @param longitude The longitude of location
 */
-(void)setGPSLatitude:(float)latitude andLongitude:(float)longitude;

/**
 Remove reload notification for banner
 */
-(void)removeReloadNotification;

/**
 Return the banner view for  UITableViewCell
 @returns a UIView contain the banner
 */
- (UIView *)viewForCell;

/**
 Return the banner size 
 @returns a CGSize contain the size of banner
 */
- (CGSize)bannerSize;

@end
