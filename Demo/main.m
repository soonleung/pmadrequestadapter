//
//  main.m
//  AdManager
//
//  Created by Soon Leung on 3/3/14.
//  Copyright (c) 2014 Pixel Media HK Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DemoAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DemoAppDelegate class]));
    }
}
