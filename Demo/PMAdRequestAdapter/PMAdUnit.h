//
//  PMAdUnit.h
//  Version 0.1
//
//  Created by Soon Leung on 3/3/14.
//  Copyright (c) 2014 Pixel Media HK Limited. All rights reserved.
//
// This code is distributed under the terms and conditions of the MIT license.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <UIKit/UIKit.h>
#import "PMAdUnitDelegate.h"
#import "PMAdRequestSource.h"

#ifdef DEBUG
#   define PMDLog(fmt, ...) {NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);}
#else
#   define PMDLog(...)
#endif

/**
 Constants to identify the size of an ad unit.
 */
typedef NS_ENUM(NSInteger, PMAdUnitSize) {
    /**
     Typically 320 points by 50 points in size. This size is used for portrait advertisements.
     */
    PMAdUnitSizeBanner = 0,
    /**
     Full screen advertisement.
     */
    PMAdUnitSizeFullscreen
};

/**
 The animation applied to the ad unit as it is hidden or made visible.
 */
typedef NS_ENUM(NSInteger, PMAdUnitAnimation) {
    /**
     No animation is applied to the ad unit as it is shown or hidden.
     */
    PMAdUnitAnimationNone = 0,
    /**
     The ad unit fades in and out as it is shown or hidden, respectively.
     */
    PMAdUnitAnimationFade,
    /**
     The ad unit slides in or out as it is shown or hidden, respectively.
     */
    PMAdUnitAnimationSlide
};

/**
 Options dictionary keys for Ad Networking configurations, for use with adUnit:configurationsForAdRequestClass:
 */
extern NSString * const PMPublisherIdKey;       // Your Publisher ID, provided by an ad network in order to serve your ad inventory

@class PMAdUnit;
@protocol PMAdRequestSource;

/**
 An container view for any ad networks with requests conform to the PMAdRequestAdapter protocol.
 */
@interface PMAdUnit : UIView
{
    
}

/////////////////////////////////////////////////////////////////////////////
/// @name Properties
/////////////////////////////////////////////////////////////////////////////

@property (nonatomic, readonly, copy) NSString      *identifier;
@property (nonatomic, weak) id <PMAdRequestSource>  requestSource;
@property (nonatomic, weak) id <PMAdUnitDelegate>   delegate;
@property (nonatomic, weak) UIViewController        *parentViewController;

@property (nonatomic, readonly) BOOL                isVisible;
@property (nonatomic, readonly) PMAdUnitSize        size;
@property (nonatomic, readonly) PMAdUnitPosition    position;

/////////////////////////////////////////////////////////////////////////////
/// @name Instance Methods
/////////////////////////////////////////////////////////////////////////////

/**
 Initializes an ad unit with the specified size.
 @param size The required size of the ad unit
 @return An initialized PMAdUnit object.
 */
- (instancetype)initWithSize:(PMAdUnitSize)size;

/**
 Initializes an ad unit with the specified size.
 @param size The required size of the ad unit.
 @param viewController The required view controller for displaying the ad unit.
 @return An initialized PMAdUnit object.
 */
- (instancetype)initWithSize:(PMAdUnitSize)size parentViewController:(UIViewController *)viewController;

/**
 Starts ad requests.
 */
- (void)startRequests;

/**
 Reloads everything from scratch.
 */
- (void)reloadRequests;

/**
 Stop all running requests.
 */
- (void)stopRequests;

/**
 Displays a banner ad that originates from the specified view.
 @param view The view from which the ad unit originates.
 @param animation The animation applied to the ad unit as it is made visible.
 */
- (void)showInView:(UIView *)view withAnimation:(PMAdUnitAnimation)animation;

/**
 Hides the ad unit.
 @param animation The animation applied to the ad unit as it is hidden.
 */
- (void)hideWithAnimation:(PMAdUnitAnimation)animation;

@end