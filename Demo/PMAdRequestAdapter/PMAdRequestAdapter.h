//
//  PMAdRequestAdapter.h
//  Version 0.1
//
//  Created by Soon Leung on 3/3/14.
//  Copyright (c) 2014 Pixel Media HK Limited. All rights reserved.
//
// This code is distributed under the terms and conditions of the MIT license.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <Foundation/Foundation.h>

typedef void (^PMAdRequestCompletionHandler)(BOOL success);
typedef void (^PMAdRequestActionHandler)(BOOL isBeingDismissed);

@class PMAdUnit;

/**
 This Adapter allows different ad network classes with incompatible interfaces to work together by exposing a standard interface to interact with.
 */
@protocol PMAdRequestAdapter <NSObject>

@required

/**
 The actual banner view that will be displaying ads for the ad vendor.
 */
@property (nonatomic, strong) UIView *bannerView;

/**
 A boolean flag that is set whenever an ad for this adapter is visible on screen. The contents of this is managed by the ad controller.
 */
@property (nonatomic) BOOL isVisible;

/**
 The completion handler to execute after the ad request is finished.
 */
@property (nonatomic, copy) PMAdRequestCompletionHandler completionHandler;

/**
 The action handler to execute after the user interacts with the ad banner or interstitial
 */
@property (nonatomic, copy) PMAdRequestActionHandler actionHandler;

/**
 Represents the error generated due to invalid request parameters.
 */
@property (nonatomic, strong) NSError *error;

@optional

/**
 Lays out the banner view for a given orientation. Does not necessarily need to set the frame's origin correctly, just the banner's size. The banner's position is set by the ad controller.
 @param interfaceOrientation The interface orientation to layout the banner view for
 */
- (void)layoutBannerForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;

/**
 Class method that determines if an ad adapter requires a publisher id in order to begin serving ads.
 */
+ (BOOL)requiresPublisherId;

/** Class method that outlines if an ad adapter requires a parent view controller in order to be displayed.
 */
+ (BOOL)requiresParentViewController;

/** 
 Starts ad request. This method is optional because not all ad networks have a the concept of "start" or "pause". If they are allocated and alive, they are service ads indefinitely until deallocated.
 @discussion If you implement startWithAdUnit:completionHandler:, then you must also implement stop and vice versa. There's no point in implementing either if you don't implement both.
 @param adUnit The ad unit object.
 @param completionHandler The block to invoke when loading is finished.
 @param actionHandler The block to invoke when the user inteacting with the ad unit.
 */
- (void)startWithAdUnit:(PMAdUnit*)adUnit
           onCompletion:(PMAdRequestCompletionHandler)completionHandler
               onAction:(PMAdRequestActionHandler)actionHandler;

/**
 Pauses ad request so that another ad network can begin displaying ads. This method is optional because not all ad networks have a concept of "pause". Ad networks that do not have this concept are simply deallocated and cleaned up when they are needed to be "paused".
 @discussion If you implement stop, then you must also implement startAdRequests and vice versa. There's no point in implementing either if you don't implement both.
 */
- (void)stop;

/**
 Returns a BOOL letting the ad controller know whether or not it can destroy an ad banner. This method is present in order to prevent an ad banner from being cleaned up before it is allowed (like when displaying an interstitial or the user is interacting with an ad). When this method is queried and returns NO, it is placed in a queue of ad adapters to be cleaned up at a later time.
 */
- (BOOL)canDestroyAdBanner;

/**
 A developer-friendly name to give your ad adapter class for easy debugging.
 */
- (NSString *)friendlyNetworkDescription;

/**
 The publisher id for the ad network. This is optional because not all ad network require a publisher id (iAds, for example).
 */
@property (copy, nonatomic) NSString *publisherId;

/**
 The parent view controller that is managing the view that the ad is currently living in.
 */
@property (strong, nonatomic) UIViewController *parentViewController;

@end