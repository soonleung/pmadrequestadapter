//
//  PMAdRequestSource.h
//  PMAdRequestAdapter
//
//  Created by Soon Leung on 14/5/14.
//  Copyright (c) 2014 Pixel Media HK Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PMAdUnit;

/**
 The PMAdUnitRequestSource protocol is adopted by an object that provides ad request classes to the ad unit on an as-needed basis, in order to arrange priorities of ad networks.
 */
@protocol PMAdRequestSource<NSObject>

/////////////////////////////////////////////////////////////////////////////
/// @name Providing ad requests
/////////////////////////////////////////////////////////////////////////////

@required

/**
 Asks the source to return the next class of ad request after the given one.
 @param adUnit The ad unit instantce requesting this information.
 @param class The given class of ad request.
 @return The class after the given ad request class, or nil to indicate that there is no next ad request.
 */
- (Class)adRequestClassForAdUnit:(PMAdUnit *)adUnit afterAdRequestClass:(Class)class;

/**
 Asks the source to return the configurations of a given class of ad request.
 @param adUnit The ad unit instantce requesting this information.
 @param class The given class of ad request.
 @return The class after the given ad request class, or nil to indicate that there is no options.
 */
- (NSDictionary *)adUnit:(PMAdUnit *)adUnit configurationsForAdRequestClass:(Class)class;

@end
