//
//  PMAdUnitDelegate.h
//  PMAdRequestAdapter
//
//  Created by Soon Leung on 14/5/14.
//  Copyright (c) 2014 Pixel Media HK Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Constants to identify the position of an ad banner.
 */
typedef NS_ENUM(NSInteger, PMAdUnitPosition) {
    /**
     Specifies that the ad unit is at the top of the view it originates from.
     */
    PMAdUnitPositionTop = 0,
    /**
     Specifies that the ad unit is at the bottom of the view it originates from.
     */
    PMAdUnitPositionBottom
};

@class PMAdUnit;

/**
 The PMAdUnitDelegate protocol defines optional methods for a delegate of a PMAdUnit object. The PMAdUnit class provides the ability for the user to specify size, position, and animations to the ad unit; this process is referred to as customizing the ad unit. It also defines methods that a delegate of a PMAdUnit object can optionally implement to intervene when ad request is loaded.
 */
@protocol PMAdUnitDelegate<NSObject>

/////////////////////////////////////////////////////////////////////////////
/// @name Customizing an ad unit
/////////////////////////////////////////////////////////////////////////////

@optional

/**
 Asks the delegate to return the position of a given ad unit. (only works with Banner Ads)
 @param adUnit The ad unit instantce requesting this information.
 @return The required position of the ad unit.
 */
- (PMAdUnitPosition)positionForAdUnit:(PMAdUnit *)adUnit;

/**
 Asks the delegate to return the image of a given ad unit. (only works with Fullscreen Ads)
 @param adUnit The ad unit instantce requesting this information.
 @return The required position of the ad unit.
 */
- (UIImage *)splashImageOnKeyWindowBeforePresentingAdUnit:(PMAdUnit *)adUnit;

/////////////////////////////////////////////////////////////////////////////
/// @name Request completion
/////////////////////////////////////////////////////////////////////////////

/**
 Tells the delegate that the ad unit has succeed loading a request.
 @param adUnit The ad unit has succeed loading.
 */
- (void)adUnitDidLoad:(PMAdUnit *)adUnit;

/**
 Tells the delegate that the ad unit has failed loading a request.
 @param adUnit The ad unit that failed to load.
 @param error The error that occurred during loading.
 */
- (void)adUnit:(PMAdUnit *)adUnit didFailLoadWithError:(NSError *)error;

/////////////////////////////////////////////////////////////////////////////
/// @name Detecting user Interactions
/////////////////////////////////////////////////////////////////////////////

/**
 Called after an ad finishes and the user come back to your application’s user interface.
 @param adUnit The ad unit object.
 */
- (void)adUnitActionDidFinish:(PMAdUnit *)adUnit;

/**
 Called just before the user clicked on an ad that will launch another application (such as the App Store)
 @param adUnit The ad unit object.
 */
- (void)adUnitActionWillLeaveApplication:(PMAdUnit *)adUnit;

@end