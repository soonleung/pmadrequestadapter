//
//  PMAdUnit.m
//  PMAdRequestAdapter
//
//  Created by Soon Leung on 7/3/14.
//  Copyright (c) 2014 Pixel Media HK Limited. All rights reserved.
//

#import "objc/runtime.h"
#import "PMAdUnit.h"
#import "PMAdRequestAdapter.h"
#import "SnapmobileAdRequest.h"

NSString * const PMPublisherIdKey = @"PMPublisherIdKey";

typedef NS_ENUM(NSInteger, PMAdUnitCoreLoadState) {
    PMAdUnitCoreLoadStateConfigurationError = 0,
    PMAdUnitCoreLoadStateSucceed,
    PMAdUnitCoreLoadStateFailed
};

/**
 All operations to manage multiple ad networks go HERE.
 */
@interface PMAdUnitCore : NSObject

@property (nonatomic) NSInteger indexOfCurrentRequest;
@property (nonatomic, weak) PMAdUnit *adUnit;
@property (nonatomic, strong) NSMutableArray        *requestClasses;
@property (nonatomic, strong) NSMutableDictionary   *requestOptions;
@property (nonatomic, strong) NSMutableDictionary   *requestInstances;

- (void)startLoadingWithCompletionHandler:(void (^) (PMAdUnitCoreLoadState loadState, id request, NSDictionary *errorInfo))completionHandler
                            actionHandler:(PMAdRequestActionHandler)actionHandler;
- (void)stopLoading;

@end

@implementation PMAdUnitCore

- (instancetype)init
{
    self = [super init];
    if (self) {
        // Initialization code
        self.indexOfCurrentRequest = 0;
        
        self.requestClasses = [[NSMutableArray alloc] initWithObjects:[SnapmobileAdRequest class], nil];
        self.requestOptions = [[NSMutableDictionary alloc] init];
        self.requestInstances = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)dealloc
{
    PMDLog(@"");
}

- (NSDictionary *)_configureRequestInstance:(id <PMAdRequestAdapter>)instance
{
    if ([instance respondsToSelector:@selector(stop)] &&
        ![instance respondsToSelector:@selector(startWithAdUnit:onCompletion:onAction:)])
    {
        PMDLog(@"You should better implement %@ in addition to %@ to be consistent. Otherwise, there has no means to restart the ads requests.", NSStringFromSelector(@selector(startWithAdUnit:onCompletion:onAction:)), NSStringFromSelector(@selector(stop)));
    }
    
    /////////////////////////////////////////////////////////////////////////////
    /// Check the existence of publish if the request adapter need one
    /////////////////////////////////////////////////////////////////////////////
    
    Method requiresPublisherId = class_getClassMethod([instance class], @selector(requiresPublisherId));
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
// Let clang know I know what I'm doing
    
    if (requiresPublisherId)
    {
        if ([[instance class]  performSelector:method_getName(requiresPublisherId)])
        {
            NSString *publisherId = self.requestOptions[NSStringFromClass([instance class])][PMPublisherIdKey];
            
            if ([publisherId length] > 0)
            {
                [instance setPublisherId:publisherId];
            }
            else{
                return @{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Ad request adapter %@ requires a publisher ID, but none was specified when instance was initialized! Please configure a publisher ID with the key 'PMPublisherIdKey' using %@", NSStringFromClass([instance class]), NSStringFromSelector(@selector(adUnit:configurationsForAdRequestClass:))]};
            }
        }
    }
    
    /////////////////////////////////////////////////////////////////////////////
    /// Check the existence of view controller for presenting if the request adapter need one
    /////////////////////////////////////////////////////////////////////////////
    
    Method requiresParentViewController = class_getClassMethod([instance class], @selector(requiresParentViewController));
    if (requiresParentViewController)
    {
        if ([[instance class] performSelector:method_getName(requiresParentViewController)])
        {
            UIViewController *parentViewController = self.adUnit.parentViewController;
            if (parentViewController)
            {
                [instance setParentViewController:parentViewController];
            }
            else
            {
                return @{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Ad request adapter %@ requires a view controller to present from, but none was specified when instance was initialized! Please set a view controller using %@", NSStringFromClass([instance class]), NSStringFromSelector(@selector(initWithSize:parentViewController:))]};
            }
        }
    }

#pragma clang diagnostic pop
    
    return nil;
}

- (void)startLoadingWithCompletionHandler:(void (^) (PMAdUnitCoreLoadState loadState, id request, NSDictionary *errorInfo))completionHandler
                            actionHandler:(PMAdRequestActionHandler)actionHandler

{
    Class currentClass = [self.requestClasses lastObject];
    NSAssert1(class_conformsToProtocol(currentClass, @protocol(PMAdRequestAdapter)), @"Registered class does not conform to %@ protocol", NSStringFromProtocol(@protocol(PMAdRequestAdapter)));
    
    NSObject <PMAdRequestAdapter> *requestInstance = self.requestInstances[NSStringFromClass(currentClass)];
    
    if ([self.requestClasses count] > self.indexOfCurrentRequest)
    {
        /////////////////////////////////////////////////////////////////////////////
        /// Asks for configurations
        /////////////////////////////////////////////////////////////////////////////
        NSDictionary *configurations = [self.adUnit.requestSource performSelector:@selector(adUnit:configurationsForAdRequestClass:)
                                                                       withObject:self
                                                                       withObject:currentClass];
        
        if (!self.requestOptions[NSStringFromClass(currentClass)] && configurations)
        {
            self.requestOptions[NSStringFromClass(currentClass)] = configurations;
        }
        
        /////////////////////////////////////////////////////////////////////////////
        /// Initialize and set configurations
        /////////////////////////////////////////////////////////////////////////////
        if (!requestInstance)
        {
            requestInstance = [[currentClass alloc] init];
            self.requestInstances[NSStringFromClass(currentClass)] = requestInstance;
        }
        
        NSDictionary *configErrInfo = [self _configureRequestInstance:requestInstance];
        if (configErrInfo)
        {
            completionHandler(PMAdUnitCoreLoadStateConfigurationError, requestInstance, configErrInfo);
        }
        else
        {
            /////////////////////////////////////////////////////////////////////////////
            /// Start loading ONLY if developeres have set configurations properly
            /////////////////////////////////////////////////////////////////////////////
            [requestInstance startWithAdUnit:self.adUnit
                                onCompletion:^(BOOL success) {
                                    
                                    if (success)
                                    {
                                        completionHandler(PMAdUnitCoreLoadStateSucceed, requestInstance, nil);
                                    }
                                    else
                                    {
                                        /////////////////////////////////////////////////////////////////////////////
                                        /// Asks for next request classe
                                        /////////////////////////////////////////////////////////////////////////////
                                        Class nextClass = [self.adUnit.requestSource performSelector:@selector(adRequestClassForAdUnit:afterAdRequestClass:)
                                                                                          withObject:self
                                                                                          withObject:currentClass];
                                        if (nextClass)
                                        {
                                            [self.requestClasses addObject:nextClass];
                                        }
                                        self.indexOfCurrentRequest++;
                                        [self startLoadingWithCompletionHandler:completionHandler actionHandler:actionHandler];
                                    }
                                    
                                }
                                    onAction:actionHandler];
        }
    }
    else
    {
        /////////////////////////////////////////////////////////////////////////////
        /// None of the ad requests succeed at all
        /////////////////////////////////////////////////////////////////////////////
        completionHandler(PMAdUnitCoreLoadStateFailed, requestInstance, nil);
    }
}

- (void)stopLoading
{
    [self.requestInstances enumerateKeysAndObjectsUsingBlock:^(id key, NSObject <PMAdRequestAdapter> *obj, BOOL *stop) {
        
        if ([obj respondsToSelector:@selector(stop)])
        {
            [obj stop];
        }
        
        obj.parentViewController = nil;
        obj.completionHandler = nil;
        obj.actionHandler = nil;
        
    }];
}

@end

@interface PMAdUnit ()
{
    PMAdUnitCore *_coreInstance;
    BOOL _originalStatusBarStatus;
}

@property (nonatomic, copy) NSString     *identifier;
@property (nonatomic, strong) UIImageView *splash;

@property (nonatomic, readwrite) BOOL               isVisible;
@property (nonatomic, readwrite) PMAdUnitSize       size;
@property (nonatomic, readwrite) PMAdUnitPosition   position;

@end

@implementation PMAdUnit

#pragma mark - Instance Methods

// Secondary initializer
- (instancetype)initWithFrame:(CGRect)frame
{
    return [self initWithSize:PMAdUnitSizeBanner];
}

// Secondary initializer
- (instancetype)initWithSize:(PMAdUnitSize)size
{
    return [self initWithSize:size parentViewController:nil];
}

// Secondary initializer
- (instancetype)initWithSize:(PMAdUnitSize)size parentViewController:(UIViewController *)viewController
{
    return [self initWithFrame:CGRectZero size:size parentViewController:viewController];
}

// Designated initializer
- (instancetype)initWithFrame:(CGRect)frame size:(PMAdUnitSize)size parentViewController:(UIViewController *)viewController
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // Initialization code
        _coreInstance = [[PMAdUnitCore alloc] init];
        _coreInstance.adUnit = self;
    
        self.clipsToBounds = YES;
        self.size = size;
        self.parentViewController = viewController;
    }
    return self;
}

- (void)dealloc
{
    PMDLog(@"");
}

#pragma mark - Operating ad requests

- (void)startRequests
{
    if ([self _shouldDisplaySplash])
    {
        [self _displaySplash];
    }
    
    /////////////////////////////////////////////////////////////////////////////
    /// Handle all delegations HERE
    /////////////////////////////////////////////////////////////////////////////
    [_coreInstance startLoadingWithCompletionHandler:^(PMAdUnitCoreLoadState loadState, NSObject <PMAdRequestAdapter> *request, NSDictionary *errorInfo) {
        
        if (loadState == PMAdUnitCoreLoadStateConfigurationError)
        {
            if ([self _shouldDisplaySplash])
            {
                [self _restoreFromSplash];
            }
            
            PMDLog(@"%@", errorInfo);
            if ([self.delegate respondsToSelector:@selector(adUnit:didFailLoadWithError:)])
            {
                NSError *error = [[NSError alloc] initWithDomain:@"com.snapmobile.pmadrequestadapter"
                                                            code:NSFormattingError
                                                        userInfo:errorInfo];
                [self.delegate adUnit:self didFailLoadWithError:error];
            }
        }
        else if (loadState == PMAdUnitCoreLoadStateSucceed)
        {
            // Layout the ad unit
            if (self.size == PMAdUnitSizeBanner)
            {
                for (UIView *aSubview in self.subviews) {
                    [aSubview removeFromSuperview];
                }
                [self addSubview:request.bannerView];
                
                CGRect frame = CGRectZero;
                frame.size = request.bannerView.frame.size;
                self.frame = frame;
            }
            
            if ([self.delegate respondsToSelector:@selector(adUnitDidLoad:)])
            {
                [self.delegate adUnitDidLoad:self];
            }
        }
        else if (loadState == PMAdUnitCoreLoadStateFailed)
        {
            if ([self _shouldDisplaySplash])
            {
                [self _restoreFromSplash];
            }
            
            if ([self.delegate respondsToSelector:@selector(adUnit:didFailLoadWithError:)])
            {
                [self.delegate adUnit:self didFailLoadWithError:request.error];
            }
        }
        
    }
                                       actionHandler:^(BOOL isBeingDismissed) {
                                           if (isBeingDismissed)
                                           {
                                               if ([self _shouldDisplaySplash])
                                               {
                                                   [self _restoreFromSplash];
                                               }
                                               
                                               if ([self.delegate respondsToSelector:@selector(adUnitActionDidFinish:)])
                                               {
                                                   [self.delegate adUnitActionDidFinish:self];
                                               }
                                           }
                                           else
                                           {
                                               if ([self.delegate respondsToSelector:@selector(adUnitActionWillLeaveApplication:)])
                                               {
                                                   [self.delegate adUnitActionWillLeaveApplication:self];
                                               }
                                           }
                                           
                                       }];
}

- (void)reloadRequests
{
    [_coreInstance stopLoading];
    _coreInstance.indexOfCurrentRequest = 0;
    [_coreInstance.requestClasses removeObjectsInRange:NSMakeRange(1, [_coreInstance.requestClasses count]-1)];
    [_coreInstance.requestOptions removeAllObjects];
    [_coreInstance.requestInstances removeAllObjects];
    [self startRequests];
}

- (void)stopRequests
{
    [_coreInstance stopLoading];
}

#pragma mark - Displaying an ad unit

- (void)_displaySplash
{
    // Remember whether the status bar was hidden or not.
    _originalStatusBarStatus = [UIApplication sharedApplication].statusBarHidden;
    [UIApplication sharedApplication].statusBarHidden = YES;
    
    [[UIApplication sharedApplication].delegate.window addSubview:self.splash];
}

- (void)_restoreFromSplash
{
    [self.splash removeFromSuperview];
    
    // Restore status bar to state it was before hiding.
    [UIApplication sharedApplication].statusBarHidden = _originalStatusBarStatus;
}

- (void)showInView:(UIView *)view withAnimation:(PMAdUnitAnimation)animation
{
    if (self.size == PMAdUnitSizeBanner)
    {
        [view addSubview:self];
        
        if (animation == PMAdUnitAnimationSlide)
        {
            CGRect frame = self.frame;
            frame.origin.y = (self.position == PMAdUnitPositionTop) ? -CGRectGetHeight(self.frame) : CGRectGetHeight(view.frame);
            self.frame = frame;
            
            self.alpha = 1;
            [UIView beginAnimations:@"slideAdBannerIn" context:NULL];
            CGFloat offset =  CGRectGetHeight(self.frame);
            self.frame = CGRectOffset(self.frame, 0,  (self.position == PMAdUnitPositionTop) ? offset : -offset);
            [UIView commitAnimations];
        }
        else
        {
            CGRect frame = self.frame;
            frame.origin.y = (self.position == PMAdUnitPositionTop) ? 0.0f : CGRectGetHeight(view.frame)-CGRectGetHeight(self.frame);
            self.frame = frame;
            
            if (animation == PMAdUnitAnimationFade)
            {
                self.alpha = 0;
                [UIView beginAnimations:@"fadeAdBannerIn" context:NULL];
                self.alpha = 1;
                [UIView commitAnimations];
            }
        }
        
        self.isVisible = YES;
    }
}

- (void)hideWithAnimation:(PMAdUnitAnimation)animation
{
    if (self.size == PMAdUnitSizeBanner)
    {
        self.isVisible = NO;

        void (^animBlock)(void);
        animBlock = ^ {
            
            if (animation == PMAdUnitAnimationSlide)
            {
                CGFloat offset =  CGRectGetHeight(self.frame);
                self.frame = CGRectOffset(self.frame, 0, (self.position == PMAdUnitPositionTop) ? -offset : offset);
            }
            else if (animation == PMAdUnitAnimationFade)
            {
                self.alpha = 0;
            }
            
        };
        
        [UIView animateWithDuration:0.3f animations:animBlock completion:^(BOOL finish) {
           
            [self removeFromSuperview];
            
        }];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - Getters
- (UIViewController *)parentViewController
{
    if (!_parentViewController)
    {
        for (UIView *next = [self superview]; next; next = next.superview) {
            UIResponder *nextResponder = [next nextResponder];
            
            if ([nextResponder isKindOfClass:[UIViewController class]])
            {
                _parentViewController = (UIViewController *)nextResponder;
                break;
            }
        }
    }
    
    return _parentViewController;
}
- (PMAdUnitPosition)position
{
    PMAdUnitPosition adPosition = PMAdUnitPositionTop;
    SEL selector = @selector(positionForAdUnit:);
    if ([self.delegate respondsToSelector:selector])
    {
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:
                                    [[self.delegate class] instanceMethodSignatureForSelector:selector]];
        [invocation setSelector:selector];
        [invocation setTarget:self.delegate];
        [invocation invoke];
        [invocation getReturnValue:&adPosition];
    }
    return adPosition;
}
- (BOOL)_shouldDisplaySplash
{
    if ([self.delegate respondsToSelector:@selector(splashImageOnKeyWindowBeforePresentingAdUnit:)] && self.size == PMAdUnitSizeFullscreen)
    {
        UIImage *splashImg = [self.delegate performSelector:@selector(splashImageOnKeyWindowBeforePresentingAdUnit:)];
        if (splashImg)
        {
            self.splash = [[UIImageView alloc] initWithImage:splashImg];
            return YES;
        }
    }

    return NO;
}

@end