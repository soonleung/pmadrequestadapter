//
//  AdMobRequest.m
//  Version 0.1
//
//  Created by Soon Leung on 3/3/14.
//  Copyright (c) 2014 Pixel Media HK Limited. All rights reserved.
//

#import "AdMobRequest.h"
#import "PMAdUnit.h"

@interface AdMobRequest ()
@property (nonatomic, strong) GADInterstitial *interstitial;
@end

@implementation AdMobRequest

#pragma mark - Required Adapted Implementation
- (GADBannerView *)bannerView
{
    if (_bannerView == nil && _publisherId)
    {
        _bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
        _bannerView.adUnitID = self.publisherId;
        _bannerView.delegate = self;
    }
    else if (!_publisherId)
    {
        PMDLog(@"Google Ad Publisher ID not set. No ads will be served until you set one using %@!", NSStringFromSelector(@selector(adUnit:configurationsForAdRequestClass:)));
    }
    
    return _bannerView;
}

- (void)layoutBannerForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    if (UIInterfaceOrientationIsLandscape(interfaceOrientation)) {
        if (GADAdSizeEqualToSize(self.bannerView.adSize, kGADAdSizeSmartBannerLandscape) == NO) {
            self.bannerView.adSize = kGADAdSizeSmartBannerLandscape;
        }
    }
    else if (GADAdSizeEqualToSize(self.bannerView.adSize,kGADAdSizeSmartBannerPortrait) == NO){
            self.bannerView.adSize = kGADAdSizeSmartBannerPortrait;
    }
    
    [self.bannerView setNeedsLayout];
}

- (void)dealloc
{
    PMDLog(@"");
}

#pragma mark - Optional Adapter Implementation
+ (BOOL)requiresPublisherId
{
    return YES;
}

+ (BOOL)requiresParentViewController
{
    return YES;
}

- (void)setParentViewController:(UIViewController *)viewController{
    _parentViewController = viewController;    
    self.bannerView.rootViewController = viewController;
    
    [self layoutBannerForInterfaceOrientation:viewController.interfaceOrientation];
}

- (void)setPublisherId:(NSString *)publisherId
{
    _publisherId = [publisherId copy];
    
    if (_bannerView != nil)
    {
        self.bannerView.adUnitID = self.publisherId;
    }
}

- (NSString *)friendlyNetworkDescription
{
    return @"AdMob by Google";
}

- (void)startWithAdUnit:(PMAdUnit*)adUnit
           onCompletion:(PMAdRequestCompletionHandler)completionHandler
               onAction:(PMAdRequestActionHandler)actionHandler
{
    self.completionHandler = completionHandler;
    self.actionHandler = actionHandler;

    GADRequest *request = [GADRequest request];

#if TARGET_IPHONE_SIMULATOR
    request.testDevices = @[GAD_SIMULATOR_ID];
#endif
    
    if (adUnit.size == PMAdUnitSizeBanner)
    {
        [self.bannerView loadRequest:request];
    }
    else if (adUnit.size == PMAdUnitSizeFullscreen)
    {
        self.interstitial = [[GADInterstitial alloc] init];
        self.interstitial.adUnitID = self.publisherId;
        self.interstitial.delegate = self;
        
        [self.interstitial loadRequest:request];
    }
}

#pragma mark - GADBannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)bannerView
{
    self.error = nil;
    if (self.completionHandler)
    {
        self.completionHandler(YES);
        self.completionHandler = nil;
    }
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error
{
    self.error = error;
    if (self.completionHandler)
    {
        self.completionHandler(NO);
        self.completionHandler = nil;
    }
}

- (void)adViewDidDismissScreen:(GADBannerView *)adView
{
    if (self.actionHandler)
    {
        self.actionHandler(YES);
        self.actionHandler = nil;
    }
}

- (void)adViewWillLeaveApplication:(GADBannerView *)adView
{
    if (self.actionHandler)
    {
        self.actionHandler(NO);
        self.actionHandler = nil;
    }
}

#pragma mark - GADInterstitialDelegate
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad
{
    self.error = nil;
    if (self.completionHandler)
    {
        self.completionHandler(YES);
    }
    
    [self.interstitial presentFromRootViewController:self.parentViewController];
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error
{
    self.error = error;
    if (self.completionHandler)
    {
        self.completionHandler(NO);
    }
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)ad
{
    if (self.actionHandler)
    {
        self.actionHandler(YES);
    }
}

- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad
{
    if (self.actionHandler)
    {
        self.actionHandler(NO);
    }
}

@end