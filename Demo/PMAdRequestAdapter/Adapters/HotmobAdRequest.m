//
//  SnapmobileAdapter.m
//  PMAdRequestAdapter
//
//  Created by Soon Leung on 25/3/14.
//  Copyright (c) 2014 Pixel Media HK Limited. All rights reserved.
//

#import "HotmobAdRequest.h"
#import "PMAdUnit.h"

#define HOTMOB_BANNER_RECT (CGRect)                {0, 0, 320.0f, 53.0f}

@interface HotmobAdRequest ()
@property (nonatomic, strong) HotMobBanner *bannerInstance;
@end

@implementation HotmobAdRequest

#pragma mark - Required Adapted Implementation
- (HotMobBanner *)bannerView
{
    return self.bannerInstance;
}
- (void)dealloc
{
    PMDLog(@"");
}

#pragma mark - Optional Adapter Implementation
+ (BOOL)requiresPublisherId
{
    return YES;
}

+ (BOOL)requiresParentViewController
{
    return YES;
}

- (void)setParentViewController:(UIViewController *)viewController
{
    _parentViewController = viewController;
}

- (void)setPublisherId:(NSString *)publisherId
{
    _publisherId = [publisherId copy];
}

- (NSString *)friendlyNetworkDescription
{
    return @"Hotmob Ads";
}

- (void)startWithAdUnit:(PMAdUnit*)adUnit
           onCompletion:(PMAdRequestCompletionHandler)completionHandler
               onAction:(PMAdRequestActionHandler)actionHandler
{
    self.completionHandler = completionHandler;
    self.actionHandler = actionHandler;
    
    if (!self.bannerInstance && _publisherId)
    {
        self.bannerInstance = [HotMobBanner hotMobBanner:self
                              rootController:self.parentViewController
                                            type:(adUnit.size == PMAdUnitSizeFullscreen) ? ShowUpTypeFullScreen : ShowUpTypeFromNone
                                 bannerFrame:(adUnit.size == PMAdUnitSizeFullscreen) ? CGRectZero : HOTMOB_BANNER_RECT
                                       appID:@"HKM"
                                          adCode:self.publisherId];
    }
}

#pragma mark - HotMobBannerDelegate
- (void)didLoadBanner:(HotMobBanner*)banner
{
    if (self.completionHandler)
    {
        self.completionHandler(YES);
    }
}

- (void)openInternalCallback:(HotMobBanner*)banner url:(NSString*)url
{
    if (self.actionHandler)
    {
        self.actionHandler(NO);
    }
}

- (void)didCloseInAppBrowser:(HotMobBanner*)banner isCloseByUser:(BOOL)isCloseByUser
{
    if (self.actionHandler)
    {
        self.actionHandler(YES);
    }
}

- (void)openNoAdCallback:(HotMobBanner*)banner;
{
    if (self.completionHandler)
    {
        self.completionHandler(NO);
    }
}


@end
