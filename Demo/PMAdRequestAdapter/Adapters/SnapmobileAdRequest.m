//
//  SnapmobileAdapter.m
//  PMAdRequestAdapter
//
//  Created by Soon Leung on 25/3/14.
//  Copyright (c) 2014 Pixel Media HK Limited. All rights reserved.
//

#import "SnapmobileAdRequest.h"
#import "PMAdUnit.h"

@interface SnapmobileAdRequest ()
@property (nonatomic, strong) PMAdBanner *bannerInstance;
@end

@implementation SnapmobileAdRequest

#pragma mark - Required Adapted Implementation
- (UIView *)bannerView
{
    return self.bannerInstance.view;
}

- (void)dealloc
{
    PMDLog(@"");
}

#pragma mark - Optional Adapter Implementation
+ (BOOL)requiresPublisherId
{
    return YES;
}

+ (BOOL)requiresParentViewController
{
    return YES;
}

- (void)setParentViewController:(UIViewController *)viewController
{
    _parentViewController = viewController;
    [self.bannerInstance showPoppedContentFromViewController:_parentViewController];    
}

- (void)setPublisherId:(NSString *)publisherId
{
    _publisherId = [publisherId copy];
    [self.bannerInstance setBannerID:self.publisherId];
}

- (NSString *)friendlyNetworkDescription
{
    return @"Snapmobile Ads";
}

- (void)startWithAdUnit:(PMAdUnit*)adUnit
           onCompletion:(PMAdRequestCompletionHandler)completionHandler
               onAction:(PMAdRequestActionHandler)actionHandler
{
    self.completionHandler = completionHandler;
    self.actionHandler = actionHandler;
    
    if (!self.bannerInstance)
    {
        self.bannerInstance = [[PMAdBanner alloc] initWithBannerID:self.publisherId andDelegate:self];
        [self.bannerInstance showPoppedContentFromViewController:self.parentViewController];
    }
    [self.bannerInstance loadBanner];
}

- (void)stop
{
    [self.bannerInstance showPoppedContentFromViewController:nil];
    [self.bannerInstance removeReloadNotification];
    [self.bannerInstance CancelBanner];
}

#pragma mark - PMAdBannerDelegate
- (void)bannerViewDidLoadAd:(PMAdBanner *)banner
{
    self.error = nil;
    if (self.completionHandler)
    {
        self.completionHandler(YES);
    }
}

- (BOOL)bannerViewActionShouldBegin:(PMAdBanner *)banner willLeaveApplication:(BOOL)willLeave
{
    if (willLeave && self.actionHandler)
    {
        self.actionHandler(NO);
    }
    
    return YES;
}

- (void)bannerViewActionDidFinish:(PMAdBanner *)banner
{
    if (self.actionHandler)
    {
        self.actionHandler(YES);
    }
}

- (void)bannerView:(PMAdBanner *)banner didFailToReceiveAdWithError:(NSError *)error
{
    self.error = error;
    if (self.completionHandler)
    {
        self.completionHandler(NO);
    }
}


@end
