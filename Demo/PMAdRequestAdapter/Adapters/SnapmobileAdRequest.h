//
//  SnapmobileAdapter.h
//  PMAdRequestAdapter
//
//  Created by Soon Leung on 25/3/14.
//  Copyright (c) 2014 Pixel Media HK Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMAdRequestAdapter.h"
#import "PMAdBanner.h"

@interface SnapmobileAdRequest : NSObject <PMAdRequestAdapter, PMAdBannerDelegate>

@property (nonatomic)           BOOL                isVisible;
@property (nonatomic, copy)     NSString            *publisherId;
@property (nonatomic, strong)   UIView              *bannerView;
@property (nonatomic, strong)   UIViewController    *parentViewController;

@property (nonatomic, copy)     PMAdRequestCompletionHandler    completionHandler;
@property (nonatomic, copy)     PMAdRequestActionHandler        actionHandler;
@property (nonatomic, strong)   NSError                         *error;

@end