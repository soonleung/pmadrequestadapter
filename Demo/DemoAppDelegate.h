//
//  DemoAppDelegate.h
//  AdManager
//
//  Created by Soon Leung on 3/3/14.
//  Copyright (c) 2014 Pixel Media HK Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DemoAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
