//
//  BannerAdsViewController.m
//  PMAdRequestAdapter
//
//  Created by Soon Leung on 6/3/14.
//  Copyright (c) 2014 Pixel Media HK Limited. All rights reserved.
//

#import "BannerAdsViewController.h"

#import "SnapmobileAdRequest.h"
#import "HotmobAdRequest.h"
#import "AdMobRequest.h"

typedef NS_ENUM(NSInteger, BannerScenario) {
    BannerScenario1 = 0,
    BannerScenario2,
    BannerScenario3
};

@interface BannerAdsViewController ()
@property (nonatomic, strong) PMAdUnit *adUnit;
@property (nonatomic, strong) PMAdBanner *banner;
@property (nonatomic, strong) HotMobBanner *adsBanner;

@property (nonatomic) BannerScenario currentScenario;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
- (IBAction)nextBtnAction:(id)sender;
@end

@implementation BannerAdsViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        // Custom initialization
        self.currentScenario = BannerScenario1;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.adUnit = [[PMAdUnit alloc] initWithSize:PMAdUnitSizeBanner parentViewController:self];
    self.adUnit.delegate = self;
    self.adUnit.requestSource = self;
    [self.adUnit startRequests];
    self.nextBtn.enabled = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.isMovingFromParentViewController || self.isBeingDismissed) {
        // This view controller is being popped or dismissed
        [self.adUnit stopRequests];
    }
}

- (void)dealloc
{
    NSLog(@"dealloc");
}

- (void)updateDetails
{
    self.nextBtn.enabled = YES;
    
    UITableViewCell *firstCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    firstCell.accessoryType = (self.currentScenario == BannerScenario1) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    firstCell.detailTextLabel.attributedText = [[NSAttributedString alloc]
                                                initWithString:firstCell.detailTextLabel.text
                                                attributes:@{NSStrikethroughStyleAttributeName : ((self.currentScenario == BannerScenario1) ? @0 : @1),
                                                             NSForegroundColorAttributeName : ((self.currentScenario == BannerScenario1) ? [UIColor blackColor] : [UIColor lightGrayColor])}];
    
    UITableViewCell *secondCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    secondCell.accessoryType = (self.currentScenario == BannerScenario2) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    secondCell.detailTextLabel.attributedText = [[NSAttributedString alloc]
                                                 initWithString:secondCell.detailTextLabel.text
                                                 attributes:@{NSStrikethroughStyleAttributeName : ((self.currentScenario == BannerScenario2) ? @0 : @1),
                                                              NSForegroundColorAttributeName : ((self.currentScenario == BannerScenario2) ? [UIColor blackColor] : [UIColor lightGrayColor])}];
    
}

#pragma mark - PMAdUnitDelegate
- (PMAdUnitPosition)positionForAdUnit:(PMAdUnit *)adUnit
{
    return PMAdUnitPositionBottom;
}

- (void)adUnitDidLoad:(PMAdUnit *)adUnit
{
    [self updateDetails];
    [adUnit showInView:self.view withAnimation:(self.currentScenario == BannerScenario2) ? PMAdUnitAnimationNone : PMAdUnitAnimationSlide];
}

- (void)adUnit:(PMAdUnit *)adUnit didFailLoadWithError:(NSError *)error
{
    [self updateDetails];
    [adUnit hideWithAnimation:PMAdUnitAnimationFade];
}

- (void)adUnitActionDidFinish:(PMAdUnit *)adUnit
{
    NSLog(@"adUnitActionDidFinish:");
}

- (void)adUnitActionWillLeaveApplication:(PMAdUnit *)adUnit
{
    NSLog(@"adUnitActionWillLeaveApplication:");
}

#pragma mark - PMAdRequestSource
- (Class)adRequestClassForAdUnit:(PMAdUnit *)adUnit afterAdRequestClass:(Class)class
{
    if (class == [SnapmobileAdRequest class])
    {
//        return [HotmobAdRequest class];
        return [AdMobRequest class];
    }

    return nil;
}

- (NSDictionary *)adUnit:(PMAdUnit *)adUnit configurationsForAdRequestClass:(Class)class
{
    if (class == [SnapmobileAdRequest class])
    {
        return @{PMPublisherIdKey: ((self.currentScenario == BannerScenario1) ? @"862968583354" : @"0")};
    }
    else if (class ==[HotmobAdRequest class])
    {
        return @{PMPublisherIdKey: ((self.currentScenario == BannerScenario2) ? @"hkm_main_head" : @"0")};
    }
    else if (class ==[AdMobRequest class])
    {
        return @{PMPublisherIdKey: ((self.currentScenario == BannerScenario2) ? @"a14e55c99c24b43" : @"0")};
    }

    return nil;
}

#pragma mark - IBActions
- (IBAction)nextBtnAction:(id)sender
{
    if (self.currentScenario == BannerScenario3)
    {
        self.currentScenario = BannerScenario1;
    }
    else
    {
        self.currentScenario++;
    }
    
    [self.adUnit reloadRequests];
    self.nextBtn.enabled = NO;
}

@end