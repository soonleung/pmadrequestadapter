//
//  FullscreenAdsViewController.m
//  PMAdRequestAdapter
//
//  Created by Soon Leung on 6/3/14.
//  Copyright (c) 2014 Pixel Media HK Limited. All rights reserved.
//

#import "FullscreenAdsViewController.h"

#import "SnapmobileAdRequest.h"
#import "HotmobAdRequest.h"
#import "AdMobRequest.h"

typedef NS_ENUM(NSInteger, PopupScenario) {
    PopupScenario1 = 0,
    PopupScenario2,
    PopupScenario3
};

@interface FullscreenAdsViewController ()
@property (nonatomic, strong) PMAdUnit *adUnit;
@property (nonatomic, weak) IBOutlet UIButton *nextBtn;
@property (nonatomic) PopupScenario currentScenario;
- (IBAction)nextBtnAction:(id)sender;
@end

@implementation FullscreenAdsViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        // Custom initialization
        self.currentScenario = PopupScenario1;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.nextBtn.enabled = NO;
    self.adUnit = [[PMAdUnit alloc] initWithSize:PMAdUnitSizeFullscreen parentViewController:self];
    self.adUnit.delegate = self;
    self.adUnit.requestSource = self;
    [self.adUnit startRequests];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.isMovingFromParentViewController || self.isBeingDismissed) {
        // This view controller is being popped or dismissed
        [self.adUnit stopRequests];
    }
}

- (void)dealloc
{
    NSLog(@"dealloc");
}

- (void)updateDetails
{
    self.nextBtn.enabled = YES;
    
    UITableViewCell *firstCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if (firstCell)
    {
        firstCell.accessoryType = (self.currentScenario == PopupScenario1) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
        firstCell.detailTextLabel.attributedText = [[NSAttributedString alloc]
                                                    initWithString:firstCell.detailTextLabel.text
                                                    attributes:@{NSStrikethroughStyleAttributeName : ((self.currentScenario == PopupScenario1) ? @0 : @1),
                                                                 NSForegroundColorAttributeName : ((self.currentScenario == PopupScenario1) ? [UIColor blackColor] : [UIColor lightGrayColor])}];
    }
    
    UITableViewCell *secondCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    if (secondCell)
    {
        secondCell.accessoryType = (self.currentScenario == PopupScenario2) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
        secondCell.detailTextLabel.attributedText = [[NSAttributedString alloc]
                                                     initWithString:secondCell.detailTextLabel.text
                                                     attributes:@{NSStrikethroughStyleAttributeName : ((self.currentScenario == PopupScenario2) ? @0 : @1),
                                                                  NSForegroundColorAttributeName : ((self.currentScenario == PopupScenario2) ? [UIColor blackColor] : [UIColor lightGrayColor])}];
    }

}

#pragma mark - PMAdUnitDelegate
- (void)adUnitDidLoad:(PMAdUnit *)adUnit
{
    [self updateDetails];
}

- (void)adUnit:(PMAdUnit *)adUnit didFailLoadWithError:(NSError *)error
{
    [self updateDetails];
}

- (void)adUnitActionDidFinish:(PMAdUnit *)adUnit
{
    NSLog(@"adUnitActionDidFinish:");
}

- (void)adUnitActionWillLeaveApplication:(PMAdUnit *)adUnit
{
    NSLog(@"adUnitActionWillLeaveApplication:");
}

#pragma mark - PMAdRequestSource
- (Class)adRequestClassForAdUnit:(PMAdUnit *)adUnit afterAdRequestClass:(Class)class
{
    if (class == [SnapmobileAdRequest class])
    {
        return [HotmobAdRequest class];
//        return [AdMobRequest class];
    }
    
    return nil;
}

- (NSDictionary *)adUnit:(PMAdUnit *)adUnit configurationsForAdRequestClass:(Class)class
{
    if (class == [SnapmobileAdRequest class])
    {
        return @{PMPublisherIdKey: ((self.currentScenario == PopupScenario1) ? @"7588743716910" : @"0")};
    }
    else if (class ==[HotmobAdRequest class])
    {
        return @{PMPublisherIdKey: ((self.currentScenario == PopupScenario2) ? @"hkm_pop_ip5" : @"0")};
    }
    else if (class ==[AdMobRequest class])
    {
        return @{PMPublisherIdKey: ((self.currentScenario == PopupScenario2) ? @"a15371bcc195a1a" : @"0")};
    }
    
    return nil;
}

#pragma mark - IBActions
- (IBAction)nextBtnAction:(id)sender
{
    if (self.currentScenario == PopupScenario3)
    {
        self.currentScenario = PopupScenario1;
    }
    else
    {
        self.currentScenario++;
    }
    
    [self.adUnit reloadRequests];
    self.nextBtn.enabled = NO;
}

@end